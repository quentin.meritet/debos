# Debos

Generating Debian OS with Debos.

## Installation 

To install the Debos tool, refer to : https://github.com/go-debos/debos

## Generation 

You must execute the following command to generate the image. 

```debos --debug-shell raspberrypi-debian_arm64.yaml ```

There are two differents images : `raspberrypi-debian_arm64.yaml` and `raspberrypi-domoticz_arm64.yaml`. 
The first one generate an default debian based image with arm64 architecture. 
The second generate an default debian based image with arm64 architecture with Domoticz software for home automation (https://www.domoticz.com/). 