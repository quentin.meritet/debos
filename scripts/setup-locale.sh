#!/bin/sh

set -e

# Generate locale from /etc/locale.gen
locale-gen 

# Reconfigure locales
dpkg-reconfigure -f noninteractive locales 

# Reconfigure TimeZone
dpkg-reconfigure -f noninteractive tzdata 

# Reconfigure Keyboard
setupcon

